package br.ucsal.bes20201.testequalidade.locadora.business;

import java.time.LocalDate;
import java.util.List;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;

public class LocacaoBO {

	/**
	 * Calcula o valor total da locação dos veículos para uma quantidade de dias.
	 * Veículos com mais de 5 anos de fabricação têm desconto de 10%.
	 * 
	 * @param veiculos              veículos que serão locados
	 * @param quantidadeDiasLocacao quantidade de dias de locação
	 * @return
	 */
	public static Double calcularValorTotalLocacao(List<Veiculo> veiculos, Integer quantidadeDiasLocacao) {
		Double total = 0d;
		Double valorLocacaoVeiculo;
		Integer anoAtual = LocalDate.now().getYear();

		for (Veiculo veiculo : veiculos) {
			valorLocacaoVeiculo = veiculo.getValorDiaria() * quantidadeDiasLocacao;
			if (veiculo.getAno() < anoAtual - 5) {
				valorLocacaoVeiculo *= .9;
			}
			total += valorLocacaoVeiculo;
		}
       
		return total;
	}
	

}
