package br.ucsal.bes20201.testequalidade.locadora;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import br.ucsal.bes20201.testequalidade.locadora.business.LocacaoBO;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;



public class LocacaoBOIntegradoTest {

	/**
	 * Testar o cálculo do valor de locação por 3 dias para 2 veículos com 1 ano de
	 * fabricaçao e 3 veículos com 8 anos de fabricação.
	 */
	
	
	static List<Veiculo> veiculos = new ArrayList<Veiculo>();
	
	@BeforeClass
	public static void setupClass() {
		
		VeiculoBuilder veiBuilder= VeiculoBuilder.umVeiculo().comAno(2019);
		VeiculoBuilder veiBuilder2= VeiculoBuilder.umVeiculo().comAno(2012);
		Veiculo veiculo1 = veiBuilder.mas().comValorDiaria(550.0).builder();
		Veiculo veiculo2 = veiBuilder.mas().comValorDiaria(220.0).builder();
		Veiculo veiculo3 = veiBuilder2.mas().comValorDiaria(150.0).builder();
		Veiculo veiculo4 = veiBuilder2.mas().comValorDiaria(120.0).builder();
		Veiculo veiculo5 = veiBuilder2.mas().comValorDiaria(80.0).builder();
		
		veiculos.add(veiculo1);
		veiculos.add(veiculo2);
		veiculos.add(veiculo3);
		veiculos.add(veiculo4);
		veiculos.add(veiculo5);
	}
		


	
	@Test
	public void testarCalculoValorTotalLocacao5Veiculos3Dias() {
	

	Double valorLoc = LocacaoBO.calcularValorTotalLocacao(veiculos, 3);
	Double value = 3255.0;
	assertEquals(value,valorLoc);
	
	}

}
