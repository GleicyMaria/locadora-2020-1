package br.ucsal.bes20201.testequalidade.locadora;

import br.ucsal.bes20201.testequalidade.locadora.dominio.Modelo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.Veiculo;
import br.ucsal.bes20201.testequalidade.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String PLACA = "ABC2020";

	private static final Integer ANO = 2020;

	private static final Modelo MODELO = new Modelo("renault zoe");

	private static final Double VALORDIARIA = 2D;

	private static final SituacaoVeiculoEnum SITUACAO = SituacaoVeiculoEnum.DISPONIVEL;

	
	
	private String placa = PLACA;

	private Integer ano = ANO;

	private Modelo modelo = MODELO;

	private Double valorDiaria = VALORDIARIA;

	private SituacaoVeiculoEnum situacao = SITUACAO;

	
	
	
	public VeiculoBuilder() {
	}

	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}

	public String getPlaca() {
		return placa;
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;

	}

	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;

	}

	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;

	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;

	}

	public VeiculoBuilder comSituacaoDisponivel(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.DISPONIVEL;
		return this;

	}

	public VeiculoBuilder comSituacaoLocado(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.LOCADO;
		return this;

	}

	public VeiculoBuilder comSituacaoManutencao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao.MANUTENCAO;
		return this;

	}

	public VeiculoBuilder mas() {
		return umVeiculo().comAno(ano).comModelo(modelo).comPlaca(placa).comSituacaoDisponivel(situacao);
	}

	public Veiculo builder() {
		Veiculo veiculo = new Veiculo();
		veiculo.setAno(ano);
		veiculo.setPlaca(placa);
		veiculo.setModelo(modelo);
		veiculo.setSituacao(situacao);
		veiculo.setValorDiaria(valorDiaria);
		return veiculo;
	}

}
